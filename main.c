/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// Print an appropriate error message for each malloc(3) error code
int rename_err(const char* prog_name, const char* src, const char* dest) {
	printf("%s: ", prog_name);
	switch (errno) {
	case EACCES:
		printf("permission denied");
		break;
	case EBUSY:
		printf("resource unavailable");
		break;
	case EDQUOT:
		printf("disk block quota exhausted");
		break;
	case EFAULT:
		printf("file outside accessible address space");
		break;
	case EINVAL:
		printf("cannot move %s into a subdirectory of itself", src);
		break;
	case EISDIR:
		printf("cannot overwrite directory %s with file %s", dest, src);
		break;
	case ELOOP:
		printf("too many symbolic links were encoutered");
		break;
	case EMLINK:
		printf("the maximum number of links has already been attained");
		break;
	case ENAMETOOLONG:
		printf("name of file too long");
		break;
	case ENOENT:
		printf("file does not exist");
		break;
	case ENOMEM:
		printf("insufficient kernel memory");
		break;
	case ENOSPC:
		printf("the device containing the file has no more room");
		break;
	case ENOTDIR:
		printf("cannot overwrite file %s with directory %s", dest, src);
		break;
	case ENOTEMPTY:
	case EEXIST:
		printf("%s already exists", dest);
		break;
	case EPERM:
		printf("bad user ID for sticky directory");
		break;
	case EROFS:
		printf("can't do that on a read-only file system, bucko");
		break;
	case EXDEV:
		printf("files are on different filesystems");
		break;
	default:
		printf("unknown error");
		break;
	}

	printf("\n");
	return 2;
}

// Print an appropriate error message for each malloc(3) error code
int malloc_err(const char* prog_name) {
	printf("%s: ", prog_name);
	switch (errno) {
	case ENOMEM:
		printf("out of memory");
		break;
	default:
		printf("unknown error");
		break;
	}

	printf("\n");
	return 2;
}

// Prints usage information should the user happen to make a mistake
void usage() {
	printf("Usage:\n"
		   "sw SOURCE DEST\n");
}

// Exit codes:
// 0: success
// 1: error from this program's code
// 2: error from a system call
int main(int argc, char** argv) {
	if (argc != 3) {
		usage();
		return 1;
	}

	// Allocate size for the buffer containing the temporary file name
	size_t buf_sz = 16;
	char* tmp_name = malloc(buf_sz * sizeof(char));
	if (!tmp_name)
		return malloc_err(argv[0]);
	// Initialize temporary file name to "a"
	strcpy(tmp_name, "a");

	for (size_t sz = 2; !access(tmp_name, F_OK); sz++) {
		if (sz == buf_sz) {
			// Allocate more space if we need more characters in our
			// temporary file name
			buf_sz *= 2;
			// We need to store the result of realloc in a temporary location
			// because if realloc fails, we have to free the original tmp_name
			char* tmp = realloc(tmp_name, buf_sz);
			if (!tmp) {
				free(tmp_name);
				return malloc_err(argv[0]);
			}
			tmp_name = tmp;
		}
		strcat(tmp_name, "a");
	}

	if (rename(argv[1], tmp_name)) {
		return rename_err(argv[0], argv[1], tmp_name);
	}
	if (rename(argv[2], argv[1])) {
			// Restore argv[1] to its original name
			if (rename(tmp_name, argv[1]))
				return rename_err(argv[0], tmp_name, argv[1]);
		return rename_err(argv[0], argv[2], argv[1]);
	}
	if (rename(tmp_name, argv[2])) {
			// Restore argv[2] to its original name
			if (rename(argv[1], argv[2])) {
				// Restore argv[1] to its original name
				if (rename(tmp_name, argv[1]))
					return rename_err(argv[0], tmp_name, argv[1]);
				return rename_err(argv[0], argv[1], argv[2]);
			}
		return rename_err(argv[0], tmp_name, argv[2]);
	}

	free(tmp_name);
	return 0;
}
