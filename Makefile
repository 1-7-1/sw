#	Copyright © 2021  171
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.

CFLAGS += -Wall -Wextra -Wpedantic
PREFIX = /usr/local/bin
BIN = sw

.PHONY: all install uninstall clean

all: $(BIN)

$(BIN): main.c
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

install: all
	cp $(BIN) $(PREFIX)

uninstall:
	rm -f $(PREFIX)/$(BIN)

clean:
	rm -f $(BIN)
